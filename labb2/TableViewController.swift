//
//  TableViewController.swift
//  labb2
//
//  Created by Ludvig Angenfelt on 2019-10-29.
//  Copyright © 2019 Ludvig Angenfelt. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var Workplaces: [Work] = []
    var Educations: [Education] = []
    
    var cellName: String!
    var cellDate: String!
    var cellDesc: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        for i in 0..<3 {
            let firstWorkDate = Int.random(in: 2000...2008)
            let lastWorkDate = firstWorkDate + Int.random(in: 1...10)
            
            let firstEducationDate = Int.random(in: 2000...2008)
            let lastEducationDate = firstEducationDate + Int.random(in: 1...10)
            
            let work = Work(name: "Work \(i)",
                description: "this is work number \(i). Curabitur ornare semper libero nec sodales. Nam faucibus massa nec tellus pellentesque consectetur. Nam sit amet sagittis purus, quis volutpat ex. In lobortis nunc risus, sit amet sodales erat mollis interdum. Aliquam in rhoncus libero. Morbi a magna non nisl sagittis imperdiet in quis neque. Suspendisse risus orci, commodo sit amet tempus eget, iaculis nec nulla. Fusce dignissim lorem odio, non tincidunt tellus luctus in. Aliquam bibendum iaculis libero, sit amet placerat sapien condimentum sed. Integer nisl diam, condimentum a maximus in, porttitor vel risus. In sed lacinia tellus, a volutpat erat. Aliquam erat volutpat. Donec sollicitudin, mauris in varius consequat, nisi leo ullamcorper odio, vitae tincidunt ipsum turpis vel justo. Maecenas vel vehicula massa. Curabitur eu porttitor dui. Quisque vitae turpis tincidunt purus pretium pellentesque non eget nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris malesuada pretium augue eget sagittis. Maecenas ac nisi mauris. In et accumsan orci, vel congue elit. Mauris non lacus sodales diam tempus rutrum ac sit amet mi. Nam dui nisi, tincidunt at tellus eget, malesuada eleifend purus. Sed lorem sem, scelerisque ac quam vel, ornare faucibus neque. Nulla eget diam finibus, imperdiet erat ac, cursus mauris.",
                date: "\(firstWorkDate)" + " - " + "\(lastWorkDate)")
            let education = Education(name: "Education \(i)",
                description: "this is education number \(i). Curabitur ornare semper libero nec sodales. Nam faucibus massa nec tellus pellentesque consectetur. Nam sit amet sagittis purus, quis volutpat ex. In lobortis nunc risus, sit amet sodales erat mollis interdum. Aliquam in rhoncus libero. Morbi a magna non nisl sagittis imperdiet in quis neque. Suspendisse risus orci, commodo sit amet tempus eget, iaculis nec nulla. Fusce dignissim lorem odio, non tincidunt tellus luctus in. Aliquam bibendum iaculis libero, sit amet placerat sapien condimentum sed. Integer nisl diam, condimentum a maximus in, porttitor vel risus. In sed lacinia tellus, a volutpat erat. Aliquam erat volutpat. Donec sollicitudin, mauris in varius consequat, nisi leo ullamcorper odio, vitae tincidunt ipsum turpis vel justo. Maecenas vel vehicula massa. Curabitur eu porttitor dui. Quisque vitae turpis tincidunt purus pretium pellentesque non eget nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris malesuada pretium augue eget sagittis. Maecenas ac nisi mauris. In et accumsan orci, vel congue elit. Mauris non lacus sodales diam tempus rutrum ac sit amet mi. Nam dui nisi, tincidunt at tellus eget, malesuada eleifend purus. Sed lorem sem, scelerisque ac quam vel, ornare faucibus neque. Nulla eget diam finibus, imperdiet erat ac, cursus mauris.",
                date: "\(firstEducationDate)" + " - " + "\(lastEducationDate)")
            
            Workplaces.append(work)
            Educations.append(education)
        }
        
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return Workplaces.count
        }
        else if section == 1 {
            return Educations.count
        }
        else {
            return Workplaces.count
        // just will always trigger on one of the two first if statements, added unecessary else
        // to avoid error and failed build
        }
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as? TableViewCell {
            if(indexPath.section == 0) {
                let work = Workplaces[indexPath.row]
                cell.experienceImageView?.image = UIImage(systemName: "sparkles")
                cell.nameLabel.text = work.name
                cell.dateLabel.text = work.date
            }
            else {
                let education = Educations[indexPath.row]
                cell.experienceImageView?.image = UIImage(systemName: "gear")
                cell.nameLabel.text = education.name
                cell.dateLabel.text = education.date
            }

            return cell
        }
        
        return TableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = ["Work", "Education"]
        return "\(sections[section])"
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "EnterCell", sender: indexPath)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let destination = segue.destination as? ExperienceDetailViewController, let indexPath = sender as? IndexPath {
            if indexPath.section == 0 {
                destination.name = Workplaces[indexPath.row].name
                destination.date = Workplaces[indexPath.row].date
                destination.desc = Workplaces[indexPath.row].description
                destination.image = UIImage(systemName: "sparkles")
            }
            else if indexPath.section == 1 {
                destination.name = Educations[indexPath.row].name
                destination.date = Educations[indexPath.row].date
                destination.desc = Educations[indexPath.row].description
                destination.image = UIImage(systemName: "gear")
            }
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
