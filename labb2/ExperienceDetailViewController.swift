//
//  ExperienceDetailViewController.swift
//  labb2
//
//  Created by Ludvig Angenfelt on 2019-10-29.
//  Copyright © 2019 Ludvig Angenfelt. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    
    
    var name: String!
    var desc: String!
    var date: String!
    var image: UIImage!
    
    
    @IBOutlet weak var cellName: UILabel!
    
    @IBOutlet weak var cellDate: UILabel!
    
    @IBOutlet weak var cellDesc: UILabel!
    
    @IBOutlet weak var cellImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        cellName.text = name
        cellDate.text = date
        cellDesc.text = desc
        cellImage.image = image
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
