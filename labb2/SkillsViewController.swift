//
//  SkillsViewController.swift
//  labb2
//
//  Created by Ludvig Angenfelt on 2019-11-12.
//  Copyright © 2019 Ludvig Angenfelt. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {
    
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var animatedView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        animateView()
    }
    

    @IBAction func dismissSkillsViewController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    private func animateView() {
    UIView.animateKeyframes(withDuration: 3, delay: 0.5, options: .repeat, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 2) {
                self.animatedView.backgroundColor = UIColor.yellow
                self.animatedView.transform = CGAffineTransform(scaleX: 1.5, y: 3)
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 2) {
                self.animatedView.center = self.mainView.center
            }
        
        })
    }
    
        
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
