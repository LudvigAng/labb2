//
//  model.swift
//  labb2
//
//  Created by Ludvig Angenfelt on 2019-11-05.
//  Copyright © 2019 Ludvig Angenfelt. All rights reserved.
//

import Foundation


struct Work {
    let name: String
    let description: String
    let date: String
}

struct Education {
    let name: String
    let description: String
    let date: String
}
