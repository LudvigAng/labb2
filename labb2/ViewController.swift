//
//  ViewController.swift
//  labb2
//
//  Created by Ludvig Angenfelt on 2019-10-29.
//  Copyright © 2019 Ludvig Angenfelt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func experiencebutton(_ sender: Any) {
        performSegue(withIdentifier: "workandeducation", sender: self)
    }
    
    @IBAction func skillsButton(_ sender: Any) {
        performSegue(withIdentifier: "showSkills", sender: self)
    }
}

